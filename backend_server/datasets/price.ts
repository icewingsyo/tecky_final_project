export const productPrice = [
  {
    sale_price: 199,
    original_price: 350,
    product_id: 1,
  },
  {
    sale_price: 209,
    original_price: 400,
    product_id: 2,
  },
  {
    sale_price: 219,
    original_price: 400,
    product_id: 3,
  },
  {
    sale_price: 229,
    original_price: 400,
    product_id: 4,
  },
  {
    sale_price: 159,
    original_price: 400,
    product_id: 5,
  },
  {
    sale_price: 169,
    original_price: 300,
    product_id: 6,
  },
  {
    sale_price: 99,
    original_price: 200,
    product_id: 7,
  },
];
