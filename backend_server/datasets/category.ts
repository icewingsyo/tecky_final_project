export const category = [
  {
    category_name: 'Pants',
  },
  {
    category_name: 'Bag',
  },
  {
    category_name: 'Hat',
  },
  {
    category_name: 'Socks',
  },
  {
    category_name: 'Clothing',
  },
  {
    category_name: 'Accessories',
  },
  {
    category_name: 'Skirt',
  },
  {
    category_name: 'Shoes',
  },
  {
    category_name: 'Shawls',
  },
  {
    category_name: 'Others',
  },
];
