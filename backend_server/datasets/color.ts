export const colors = [
  {
    color_name: 'red',
  },
  {
    color_name: 'blue',
  },
  {
    color_name: 'white',
  },
  {
    color_name: 'black',
  },
  {
    color_name: 'yellow',
  },
  {
    color_name: 'green',
  },
  {
    color_name: 'purple',
  },
];
