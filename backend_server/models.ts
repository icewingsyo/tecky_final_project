export interface Customer {
  id: number;
  username: string;
}

export interface Product {
  id: number;
  name: string;
}

export interface Session {
  id: number;
  customer_id: number;
  product_id: number;
  event: string;
  session_code: string;
}
