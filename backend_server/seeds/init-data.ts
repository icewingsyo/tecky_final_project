import * as Knex from 'knex';
import { tables } from '../utils/tables';
import { customers } from '../datasets/customer';
import { subCategory } from '../datasets/sub_category';
import { category } from '../datasets/category';
import { users } from '../datasets/user';
import { photos } from '../datasets/photo';
import { facebooks } from '../datasets/facebook';
import { goolges } from '../datasets/google';
import { apples } from '../datasets/apple';
import { sellers } from '../datasets/seller';
import XLSX from 'xlsx';
import path from 'path';

type InsertedUser = { id: number; email: string };
type InsertedBrand = { id: number; brand_name: string };
type productExcel = { id: number, product_id_code: number; product_name: string; brand_name: string; product_url: string; product_pic: string; color_name: string; size: string; material: string; 
  category_id: number; subcategory_id: number; gender: string; original_price: number};
type sessionExcel = { id: number, event: string; email: string; product_id_code: number; session_code: number};


export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(tables.RATING).del();
  await knex(tables.CUSTOMER_SESSION).del();
  await knex(tables.COLOR).del();
  await knex(tables.STYLE).del();
  await knex(tables.PRICE).del();
  await knex(tables.LIKE_PRODUCT).del();
  await knex(tables.LIKE_BRAND).del();
  await knex(tables.LIKE_STRING).del();
  await knex(tables.PRODUCT).del();
  await knex(tables.SELLER).del();
  await knex(tables.CUSTOMER).del();
  await knex(tables.SUB_CATEGORY).del();
  await knex(tables.CATEGORY).del();
  await knex(tables.APPLE_LOGIN).del();
  await knex(tables.GOOGLE_LOGIN).del();
  await knex(tables.FACEBOOK_LOGIN).del();
  await knex(tables.PHOTO).del();
  await knex(tables.BRAND).del();
  await knex(tables.USERS).del();

  const workbook_product = XLSX.readFile(path.join(__dirname, './FinalVersion0415.xlsx'));
  const product_data:Array<productExcel> = XLSX.utils.sheet_to_json(workbook_product.Sheets['product_data']); // get data in product excel

  const workbook_session = XLSX.readFile(path.join(__dirname, './customer_session1.xlsx'));
  const session_data:Array<sessionExcel> = XLSX.utils.sheet_to_json(workbook_session.Sheets['customer_session']);// get data in session excel

  /** ------------- Insert data start below ----------------- */
  await knex(tables.USERS).insert(users); // insert users

  // insert brand --------------------
  let strCount: string[] = [];
  for (let display of product_data) {
    if (strCount.find((brand_name) => brand_name == display.brand_name)) {
      /*do nothing */
    } else {
      strCount.push(display.brand_name);
    }
  }
  let jsonCount = strCount.map((str) => ({ brand_name: str }));
  const insertedBrand: Array<InsertedBrand> = await knex(tables.BRAND)
    .insert(jsonCount)
    .returning(['brand_name', 'id']);
  const brandMap = insertedBrand.reduce((mapping, brand) => {
    mapping.set(brand.brand_name, brand.id);
    return mapping;
  }, new Map<string, number>());
  //----------------------------------
  await knex(tables.PHOTO).insert(photos);// insert photo table
  await knex(tables.FACEBOOK_LOGIN).insert(facebooks);// insert facebook table
  await knex(tables.GOOGLE_LOGIN).insert(goolges);// insert google table
  await knex(tables.APPLE_LOGIN).insert(apples);// insert apple table
  
  await knex(tables.CATEGORY).insert(category);// insert category table

  await knex(tables.SUB_CATEGORY).insert(subCategory);// insert sub_category table

  // Inserts seed entries - customer table
  const insertedUsers: Array<InsertedUser> = await knex(tables.CUSTOMER).insert(customers).returning(['email', 'id']);
  const userMap = insertedUsers.reduce(
    (mapping, customer) => {
      mapping.set(customer.email, customer.id)
      return mapping;
    }, new Map<string, number>());

  await knex(tables.SELLER).insert(sellers);// insert seller table

  // -------insert product table and mapping(itemMap) 
  const updatedProduct = product_data.map((session) => ({
    product_id_code: session.product_id_code,
    product_name: session.product_name,
    brand_id: brandMap.get(session.brand_name),
    product_url: session.product_url,
    product_pic: session.product_pic,
    subcategory_id: session.subcategory_id,
    category_id: session.category_id,
    gender: session.gender
  }));
  
  const insertedProduct: Array<productExcel> = await knex(tables.PRODUCT)
    .insert(updatedProduct)
    .returning(['product_id_code', 'id']);
    
    const itemMap = insertedProduct.reduce((mapping, product) => {
      mapping.set(product.product_id_code, product.id);
      return mapping;
    }, new Map<number, number>());
  // ----------------------------------------------------------

  // insert price
  const updatedPrice = product_data.map((session) => ({
    original_price: session.original_price,
    product_id: itemMap.get(session.product_id_code),
  }));
  await knex(tables.PRICE).insert(updatedPrice);
  // ----------------------------------------------------------

  // insert color
  const updatedColor = product_data.map((session) => ({
    color_name: session.color_name,
    product_id: itemMap.get(session.product_id_code),
  }));
  await knex(tables.COLOR).insert(updatedColor);
  // ----------------------------------------------------------

  // insert session
  const updatedSessions = session_data.map((session) => ({
    customer_id: userMap.get(session.email),
    product_id: itemMap.get(session.product_id_code),
    session_code: session.session_code,
    event: session.event,
  }));
  await knex(tables.CUSTOMER_SESSION).insert(updatedSessions);
}

