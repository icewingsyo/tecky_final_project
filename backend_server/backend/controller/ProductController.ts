import { ProductService } from '../service/ProductService';
import { Request, Response } from 'express';

export class ProductController {
  constructor(private productService: ProductService) {}

  getAllProduct = async (req: Request, res: Response) => {
    try {
      const LIMIT:number = 30;
      const newPageNum:number = parseInt(req.params.pageNum);
      const nextPageNum:number = newPageNum + 1;
      const OFFSET:number = (LIMIT * (newPageNum - 1));

      const allProductResult = await this.productService.getAllProduct(OFFSET, LIMIT);

      if(allProductResult.length === 0){
        res.json({data: [], page: newPageNum, nextPage: newPageNum, limit: true, msg:'no more data to display'});
        return;
      }
      res.json({data: allProductResult, page: newPageNum, nextPage: nextPageNum, limit: false, msg:'product get'});
      return;
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'Server error.' });
    }
  };

  getProductByClass = async (req: Request, res: Response) => {
    try {
      const LIMIT:number = 30;
      const newPageNum:number = parseInt(req.params.pageNum);
      const nextPageNum:number = newPageNum + 1;
      const OFFSET:number = (LIMIT * (newPageNum - 1));
      const pClasses:string = req.params.pClass;

      const allProductResult = await this.productService.getProductByClass(OFFSET, LIMIT, pClasses);

      if(allProductResult.length === 0){
        res.json({data: [], page: newPageNum, nextPage: newPageNum, limit: true, msg:'no more data to display'});
        return;
      }
      res.json({data: allProductResult, page: newPageNum, nextPage: nextPageNum, limit: false, msg:'product get'});
      return;
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'Server error.' });
    }
  };

  getBrandName = async (req: Request, res: Response) => {
    try {
      const allBrand = await this.productService.getAllBrand();
      res.json({ data: allBrand, message: 'get all brand' });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'Server error.' });
    }
  };
  getRatingProduct = async (req: Request, res: Response) => {
    try {
      const LIMIT:number = 30;
      const newPageNum:number = parseInt(req.params.pageNum);
      //const nextPageNum:number = newPageNum + 1;
      const OFFSET:number = (LIMIT * (newPageNum - 1));

      const userID: number = parseInt(req.session['user'].id);
      const checkRegister = req.session['user'].statusCode; // 201, 200, 100, 250
      if (checkRegister == 201) {
        console.log('run getAllProduct');
        const allProductResult = await this.productService.getAllProduct(OFFSET, LIMIT);
        res.json({ data: allProductResult, message: 'get all product' });
        return;
      }
      console.log('run getRatingProduct');
      const allRatingProductResult = await this.productService.getRatingProduct(userID);
      res.json({ data: allRatingProductResult, message: 'get all rating product' });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'Server error.' });
    }
  };
}
