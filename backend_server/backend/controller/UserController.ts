import { UserService } from '../service/UserService';
import { Request, Response } from 'express';
import { checkPassword, hashPassword } from '../../utils/hash';
import httpStatusCodes from 'http-status-codes';
import jwtSimple from 'jwt-simple';
import jwt from '../../utils/jwt';
import { logger } from '../../utils/logger';

export class UserController {
  constructor(private userService: UserService) {}

  createCustomer = async (req: Request, res: Response) => {
    try {
      
            const {
                customer_first_name,
                customer_last_name,
                email,
                gender,
                location,
                password,
                preferred_category,
                username,
                } = req.body;
                
      const customerCount = await this.userService.checkEmailInvalid(email);
      if (customerCount[0]['count'] != 0) {
        res.status(httpStatusCodes.BAD_REQUEST).json({ message: 'duplicated user' });
        return;
      }
      const hashedPassword = await hashPassword(password);
      const userID:number = await this.userService.createUser(); // return users table id
      const photo:number = await this.userService.createPhoto();
      const facebookId:number = await this.userService.createFBLogin();
      const googleId:number = await this.userService.createGoogleLogin();
      const appleId:number = await this.userService.createAppleLogin();
      const newID = await this.userService.createCustomer(userID, customer_first_name, customer_last_name, username, email, hashedPassword, location, gender, preferred_category, photo, facebookId, googleId, appleId);
      
      res.status(200).json({ data: newID, message: 'success register' });
    } catch (err) {
      console.log(err.message);
      res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'server error!' });
    }
  };

  logout = async (req: Request, res: Response) => {
    try {
      if (req.session) {
        delete req.session['user'];
        res.redirect('/index.html'); //handle in .js
      }
    } catch (err) {
      console.log(err.message);
      res.status(httpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'server error!' });
    }
  };

  customerLogin = async (req: Request, res: Response) => {
    try {
      if (!req.body.email || !req.body.password) {
        res.status(401).json({msg:"Wrong Username/Password"});
        return;
    }
      const { email, password } = req.body;
      const userResult = await this.userService.checkEmailByLogin(email);
      
      if (!userResult) {
        res.status(400).json({ message: 'invalid email.' });
        return;
      }

      const matchPassword = await checkPassword(password, userResult.password);
      if (!matchPassword) {
        res.status(400).json({ message: 'invalid password.' });
        return;
      }
      if (userResult.is_active == false) {
        res.status(400).json({ message: 'User is not exist.' });
        return;
      }
      const payload = { id: userResult.id }; // save id to check token
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      
      res.json({ message: 'success login', status: 200, token: token });
      
    } catch (err) {
      logger.error(err.toString());
      res.status(500).json({ message: 'server error!' });
    }
  };

  getCustomer = async (req: Request, res: Response) => {
    try {
      const authCode:string = String(req.headers.authorization);
      const token:string = authCode.slice(7);
      const userID = jwtSimple.decode(token, jwt.jwtSecret);
      
      const userResult = await this.userService.getCustomerByID(userID.id);

      res.json({ data: userResult, message: 'customer data get it' });
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: 'server error!' });
    }
  };

  updateCustomerWithPw = async (req: Request, res: Response) => {
    try {
      const customerID: number = parseInt(req.session['user'].id);
      const { email, password, username, firstName, lastName, gender } = req.body;
      const hashedPassword = await hashPassword(password);
      if (!password) {
        // no need to update password
      }
      await this.userService.updateCustomerWithPw(
        customerID,
        email,
        hashedPassword,
        username,
        firstName,
        lastName,
        gender
      );
      res.json({ message: 'update success', status: 200 });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'server error!' });
    }
  };
  updateCustomerWithNoPw = async (req: Request, res: Response) => {
    try {
      const customerID: number = parseInt(req.session['user'].id);
      const { email, username, firstName, lastName, gender } = req.body;
      await this.userService.updateCustomerWithNoPw(customerID, email, username, firstName, lastName, gender);
      res.json({ message: 'update success', status: 200 });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'server error!' });
    }
  };
  getCustomerForUpdate = async (req: Request, res: Response) => {
    try {
      const customerID: number = parseInt(req.session['user'].id);
      const customerResult = await this.userService.getCustomerForUpdate(customerID);
      res.json({ data: customerResult, message: 'get customer success', status: 200 });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'server error!' });
    }
  };

  validate = async (req:Request, res:Response) =>{
    try {
      res.status(200).end();
    } catch (err) {
      logger.error(err.toString());
      res.status(500).json({ message: 'server error!' });
    }
  }
}
