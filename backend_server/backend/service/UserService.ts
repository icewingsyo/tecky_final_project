import * as Knex from 'knex';
import { Customer, Users } from '../../utils/model';
import { tables } from '../../utils/tables';
import { variables } from '../../utils/variables';

export class UserService {
  constructor(private knex: Knex) {}

  async getAllCustomer(): Promise<Customer> {
    return await this.knex.select('*').from(tables.CUSTOMER);
  }

  async getCustomerByID(id: number): Promise<Users> {
    return (await this.knex.select('users.id','users.users_is_active','users.seller_is_active', 
    'customer.customer_first_name','customer.customer_last_name','customer.username', 
    'customer.email','customer.location','customer.gender', 
    'customer.is_active','photo.photo_str','seller.pending','seller.brand_id')
    .from(tables.USERS)
    .leftJoin(`${tables.CUSTOMER}`,`${tables.CUSTOMER}.users_id`,`${tables.USERS}.id`)
    .leftJoin(`${tables.SELLER}`,`${tables.SELLER}.users_id`,`${tables.USERS}.id`)
    .leftJoin(`${tables.PHOTO}`,`${tables.PHOTO}.id`,`${tables.CUSTOMER}.photo_id`)
    .where(`${tables.CUSTOMER}.id`, id))[0];
  }

  async checkEmailInvalid(email: string) {
    return await this.knex(tables.CUSTOMER).count('*').where('email', email);
  } // customer register, check email by counting

  async checkEmailByLogin(email: string) {
    return (await this.knex.select('*').from(tables.CUSTOMER).where('email', email))[0];
  } // customer login, check email

  async createUser(){
    return (await this.knex('users').insert({users_is_active: true}).returning('id'))[0];
  }
  async createPhoto(){
    return (await this.knex('photo').insert({photo_str: null}).returning('id'))[0];
  }
  async createFBLogin(){
    return (await this.knex('facebook_login').insert({auth_code: null}).returning('id'))[0];
  }
  async createGoogleLogin(){
    return (await this.knex('google_login').insert({auth_code: null}).returning('id'))[0];
  }
  async createAppleLogin(){
    return (await this.knex('apple_login').insert({auth_code: null}).returning('id'))[0];
  }

  async createCustomer(
    users_id: number,
    customer_first_name: string,
    customer_last_name: string,
    username: string,
    email: string,
    password: string,
    location: string,
    gender: string,
    preferred_category: string,
    photo_id: number,
    facebook_id: number,
    google_id: number,
    apple_id: number,
    
    // is_active:boolean
  ) {
    return (
      await this.knex
        .insert({ users_id: users_id, customer_first_name: customer_first_name, customer_last_name: customer_last_name, 
          username: username, email: email, password: password, location: location, gender: gender, preferred_category: preferred_category, 
          photo_id: photo_id, facebook_login_id: facebook_id, google_login_id: google_id, apple_login_id: apple_id })
        .into(tables.CUSTOMER)
        .returning('id')
    )[0];
    
  } // create customer

  async createCustomerFavourite(customer_id: any) /*: Promise<CustomerFavourite>*/ {
    return await this.knex
      .insert({
        customer_id,
      })
      .into(tables.LIKE_PRODUCT);  /// need to change
  } // create customer favourite

  async createCustomerPrefer(customer_id: any) /*: Promise<CustomerPrefer>*/ {
    return await this.knex.insert({ customer_id }).into(tables.CUSTOMER_SESSION);
  }

  async updateCustomerWithPw(
    id: number,
    email: string,
    password: string,
    username: string,
    firstName: string,
    lastName: string,
    gender: string
  ) {
    return await this.knex(tables.CUSTOMER).where(variables.ID, '=', id).update({
      email: email,
      password: password,
      username: username,
      customer_first_name: firstName,
      customer_last_name: lastName,
      gender: gender,
    });
  }
  async updateCustomerWithNoPw(
    id: number,
    email: string,
    username: string,
    firstName: string,
    lastName: string,
    gender: string
  ) {
    return await this.knex(tables.CUSTOMER).where(variables.ID, '=', id).update({
      email: email,
      username: username,
      customer_first_name: firstName,
      customer_last_name: lastName,
      gender: gender,
    });
  }
  async getCustomerForUpdate(id: number) {
    return (
      await this.knex
        .select('id', 'email', 'username', 'customer_first_name', 'customer_last_name', 'gender')
        .from(tables.CUSTOMER)
        .where(variables.ID, id)
    )[0];
  }
}
