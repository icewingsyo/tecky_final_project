export const EVENT_WEIGHT = Object.freeze({
  click_on_category: 10,
  click_on_sub_category: 15,
  click_on_filtring: 20,
  click_on_search: 20,
  click_on_favourite: 25,
  click_on_product_name: 25,
});
