/*
For testing
*/
import XLSX from 'xlsx';
import path from 'path';

let strCount: string[] = [];


type InsertedProduct = { id: number; brand_name: string };
type InsertedSession = {event: string; email: string; product_id_code: number; session_code: number}

const workbook = XLSX.readFile(path.join(__dirname, '../seeds/FinalVersion0415.xlsx'))
const product_data:Array<InsertedProduct> = XLSX.utils.sheet_to_json(workbook.Sheets['product_data'])

//console.log(product_data[300])

for (let display of product_data) {
  if (strCount.find((brand_name) => brand_name == display.brand_name)) {
  } else {
    strCount.push(display.brand_name);
  }
}
let jsonCount = strCount.map((str) => ({ brand_name: str }));
//let jsonCount = JSON.stringify(strCount);
console.log(jsonCount);

const workbook2 = XLSX.readFile(path.join(__dirname, '../seeds/customer_session1.xlsx'))
const product_data2:Array<InsertedSession> = XLSX.utils.sheet_to_json(workbook2.Sheets['customer_session'])

console.log(product_data2.length);
//console.log(product_data2[5000].product_id_code);
//import jwtSimple from 'jwt-simple';
//import jwt from './jwt';
//
//let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MX0.6YSpDOtVdZAYRlIrnWs1FdKeVEw_lBm38BPnn__cXTg";
//
//const userID = jwtSimple.decode(token, jwt.jwtSecret);
//console.log(userID);
