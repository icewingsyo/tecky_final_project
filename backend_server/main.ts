import dotenv from 'dotenv';
import Knex from 'knex';
import express from 'express';
import { logger } from './utils/logger';

import { UserService } from './backend/service/UserService'; //service
import { UserController } from './backend/controller/UserController'; //controller
import { ProductService } from './backend/service/ProductService';
import { ProductController } from './backend/controller/ProductController';
import cors from 'cors';

dotenv.config();
const knexConfig = require('./knexfile');
export const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use((req, res, next) => {
  logger.info(`method: ${req.method} path: ${req.url}`);
  next();
});

// new service
const userService = new UserService(knex);
const productService = new ProductService(knex);

// new controller, call service and export to unique routes
export const userController = new UserController(userService);
export const productController = new ProductController(productService);

// call the routes include controller and service
import { userRoutes } from './backend/routers/userRoute'; //router
import { productRoutes } from './backend/routers/productRoute';

app.use(productRoutes);
app.use(userRoutes);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  logger.info(`[Info] listening to port: [${PORT}]`);
});

//console.log react = pSQL
// python > express 
// PowerBI data visualization 