window.onload = () => {
    loadAllProduct()
}

async function loadAllProduct() {
    try {
        const res = await fetch('/getAllProduct')
        const data = await res.json()
        console.log(data.data)
        let productList = ``
        let count = 1
        for (const currList of data.data) {
            productList += `
            <div class="card" style="width: 18rem;">
                <img src="${currList.product_url}" class="card-img-top" alt="...">
                <div class="card-body">
                <p class="card-text">${currList.id} ${currList.product_name}</p>
                </div>
            </div>
            `
            count += 1
        }
        document.querySelector('#productList').innerHTML = productList
    } catch (err) {
        console.log(err.message)
    }
}
