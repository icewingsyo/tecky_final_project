// window.onloadstart = loadAllBrand();
window.onload = async () => {
    await loadUpdateDetail()
    //initupdateForm();
    //await loadAllBrand();
    await loadRatingProduct()
    await initUpdateForm()
    await document.querySelector('.selected').click()
}
async function loadAllBrand() {
    const res = await fetch('/getAllBrand')
    const data = await res.json()
    console.log(data.data)
    let currBrandList = ``

    for (const currList of data.data) {
        currBrandList += `<li><input class="filter" data-filter=".check${currList.id}" type="checkbox" id="checkbox${currList.id}"><label class="checkbox-label" for="checkbox${currList.id}">${currList.brand_name}</label></li>`
    }
    document.querySelector('.checkList').innerHTML = currBrandList
} // not expect eff
async function loadRatingProduct() {
    const res = await fetch('/getRatingProduct')
    const data = await res.json()
    console.log(data.data)
    let currProductList = ``

    for (const currList of data.data) {
        let pic_split = currList.product_pic.toString().split(', ')[0]

        currProductList += `<a href="${currList.product_url}" target="_blank"><li class="mix check${currList.brand_id}"><img src="${pic_split}" alt="Product ${currList.id}"></li></a>`
    }
    currProductList += `<li class="gap"></li><li class="gap"></li><li class="gap"></li>`
    document.querySelector('.allProductList_show').innerHTML = currProductList
}
async function loadUpdateDetail() {
    const res = await fetch('/getCustomerForUpdate')
    const data = await res.json()
    console.log(data.data)
    let currCustomerList = ``
    let currList = data.data
    let form_firstName = ''
    let form_lastName = ''
    let form_username = ''
    if (currList.customer_first_name == null) {
    } else {
        form_firstName = currList.customer_first_name
    }

    if (currList.customer_last_name == null) {
    } else {
        form_lastName = currList.customer_last_name
    }

    if (currList.username == null) {
    } else {
        form_username = currList.username
    }
    console.log(form_firstName)
    console.log(form_lastName)
    console.log(form_username)
    currCustomerList += `
		<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
							for="signin-email">E-mail</label>
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-email" type="email" name="email" placeholder="E-mail" value="${currList.email}" disabled>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
							for="signin-password">Password</label>
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-password" type="text" name="password" placeholder="Password">
						<a href="#0" class="cd-signin-modal__hide-password js-hide-password">Hide</a>
					</p>
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
							for="signin-username">Username</label>
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-username" type="text" name="username" placeholder="Username" value="${form_username}" >
					</p>
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
							for="signin-firstname">First Name</label>
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-firstname" type="text" name="firstName" placeholder="First Name" value="${form_firstName}" >
					</p>
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
							for="signin-lastname">Last Name</label>
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-lastname" type="text" name="lastName" placeholder="Last Name" value="${form_lastName}" >
					</p>
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
							for="signin-gender">Gender</label>
							<select id="signin-gender" name="gender" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border">`
    if (currList.gender == 'secret') {
        currCustomerList += `<option value="secret" selected>Secret</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
				</p>

				<p class="cd-signin-modal__fieldset">
					<input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
						value="Update information">
				</p>`
    } else if (currList.gender == 'male') {
        currCustomerList += `<option value="secret" selected>Secret</option>
							<option value="male" selected>Male</option>
							<option value="female">Female</option>
						</select>
				</p>

				<p class="cd-signin-modal__fieldset">
					<input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
						value="Update information">
				</p>`
    } else if (currList.gender == 'female') {
        currCustomerList += `<option value="secret" selected>Secret</option>
								<option value="male">Male</option>
								<option value="female" selected>Female</option>
							</select>
					</p>

					<p class="cd-signin-modal__fieldset">
						<input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
							value="Update information">
					</p>
		`
    }
    document.querySelector('#updateProfile').innerHTML = currCustomerList
}
async function initUpdateForm() {
    const updateForm = document.getElementById('updateProfile')
    updateForm.addEventListener('submit', async function (event) {
        event.preventDefault()
        const formObject = {}
        formObject['email'] = updateForm.email.value
        formObject['password'] = updateForm.password.value
        formObject['username'] = updateForm.username.value
        formObject['firstName'] = updateForm.firstName.value
        formObject['lastName'] = updateForm.lastName.value
        formObject['gender'] = updateForm.gender.value
        console.log(formObject)
        let checkPw = ''
        if (updateForm.password.value == null || '') {
            checkPw = '/updateCustomerWithNoPw'
        } else {
            checkPw = '/updateCustomerWithPw'
        }
        const res = await fetch(checkPw, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            body: JSON.stringify(formObject),
        })
        if (res.status == 200) {
            const data = await res.json()
            let location = '/login_index.html'
            window.location = location
        } else {
            const data = await res.json()
            alert(data.message)
        }
    })
}
// jQuery(document).ready(function($){
// 	$(window).on("load");
// 	$(window).on("load",loadAllProduct);

// });

jQuery(document).ready(function ($) {
    //if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
    var MqL = 1170
    //move nav element position according to window width
    moveNavigation()
    $(window).on('resize', function () {
        !window.requestAnimationFrame ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation)
    })

    //mobile - open lateral menu clicking on the menu icon
    $('.cd-nav-trigger').on('click', function (event) {
        event.preventDefault()
        if ($('.cd-main-content').hasClass('nav-is-visible')) {
            closeNav()
            $('.cd-overlay').removeClass('is-visible')
        } else {
            $(this).addClass('nav-is-visible')
            $('.cd-primary-nav').addClass('nav-is-visible')
            $('.cd-main-header').addClass('nav-is-visible')
            $('.cd-main-content')
                .addClass('nav-is-visible')
                .one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $('body').addClass('overflow-hidden')
                })
            toggleSearch('close')
            $('.cd-overlay').addClass('is-visible')
        }
    })

    //open search form
    $('.cd-search-trigger').on('click', function (event) {
        event.preventDefault()
        toggleSearch()
        closeNav()
    })

    //close lateral menu on mobile
    $('.cd-overlay').on('swiperight', function () {
        if ($('.cd-primary-nav').hasClass('nav-is-visible')) {
            closeNav()
            $('.cd-overlay').removeClass('is-visible')
        }
    })
    $('.nav-on-left .cd-overlay').on('swipeleft', function () {
        if ($('.cd-primary-nav').hasClass('nav-is-visible')) {
            closeNav()
            $('.cd-overlay').removeClass('is-visible')
        }
    })
    $('.cd-overlay').on('click', function () {
        closeNav()
        toggleSearch('close')
        $('.cd-overlay').removeClass('is-visible')
    })

    //prevent default clicking on direct children of .cd-primary-nav
    $('.cd-primary-nav')
        .children('.has-children')
        .children('a')
        .on('click', function (event) {
            event.preventDefault()
        })
    //open submenu
    $('.has-children')
        .children('a')
        .on('click', function (event) {
            if (!checkWindowWidth()) event.preventDefault()
            var selected = $(this)
            if (selected.next('ul').hasClass('is-hidden')) {
                //desktop version only
                selected
                    .addClass('selected')
                    .next('ul')
                    .removeClass('is-hidden')
                    .end()
                    .parent('.has-children')
                    .parent('ul')
                    .addClass('moves-out')
                selected
                    .parent('.has-children')
                    .siblings('.has-children')
                    .children('ul')
                    .addClass('is-hidden')
                    .end()
                    .children('a')
                    .removeClass('selected')
                $('.cd-overlay').addClass('is-visible')
            } else {
                selected
                    .removeClass('selected')
                    .next('ul')
                    .addClass('is-hidden')
                    .end()
                    .parent('.has-children')
                    .parent('ul')
                    .removeClass('moves-out')
                $('.cd-overlay').removeClass('is-visible')
            }
            toggleSearch('close')
        })

    //submenu items - go back link
    $('.go-back').on('click', function () {
        $(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out')
    })

    function closeNav() {
        $('.cd-nav-trigger').removeClass('nav-is-visible')
        $('.cd-main-header').removeClass('nav-is-visible')
        $('.cd-primary-nav').removeClass('nav-is-visible')
        $('.has-children ul').addClass('is-hidden')
        $('.has-children a').removeClass('selected')
        $('.moves-out').removeClass('moves-out')
        $('.cd-main-content')
            .removeClass('nav-is-visible')
            .one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                $('body').removeClass('overflow-hidden')
            })
    }

    function toggleSearch(type) {
        if (type == 'close') {
            //close serach
            $('.cd-search').removeClass('is-visible')
            $('.cd-search-trigger').removeClass('search-is-visible')
            $('.cd-overlay').removeClass('search-is-visible')
        } else {
            //toggle search visibility
            $('.cd-search').toggleClass('is-visible')
            $('.cd-search-trigger').toggleClass('search-is-visible')
            $('.cd-overlay').toggleClass('search-is-visible')
            if ($(window).width() > MqL && $('.cd-search').hasClass('is-visible'))
                $('.cd-search').find('input[type="search"]').focus()
            $('.cd-search').hasClass('is-visible')
                ? $('.cd-overlay').addClass('is-visible')
                : $('.cd-overlay').removeClass('is-visible')
        }
    }

    function checkWindowWidth() {
        //check window width (scrollbar included)
        var e = window,
            a = 'inner'
        if (!('innerWidth' in window)) {
            a = 'client'
            e = document.documentElement || document.body
        }
        if (e[a + 'Width'] >= MqL) {
            return true
        } else {
            return false
        }
    }

    function moveNavigation() {
        var navigation = $('.cd-nav')
        var desktop = checkWindowWidth()
        if (desktop) {
            navigation.detach()
            navigation.insertBefore('.cd-header-buttons')
        } else {
            navigation.detach()
            navigation.insertAfter('.cd-main-content')
        }
    }
})

jQuery(document).ready(function ($) {
    //open/close lateral filter
    $('.cd-filter-trigger').on('click', function () {
        triggerFilter(true)
    })
    $('.cd-filter .cd-close').on('click', function () {
        triggerFilter(false)
    })

    function triggerFilter($bool) {
        var elementsToTrigger = $([$('.cd-filter-trigger'), $('.cd-filter'), $('.cd-tab-filter'), $('.cd-gallery')])
        elementsToTrigger.each(function () {
            $(this).toggleClass('filter-is-visible', $bool)
        })
    }

    //mobile version - detect click event on filters tab
    var filter_tab_placeholder = $('.cd-tab-filter .placeholder a'),
        filter_tab_placeholder_default_value = 'Select',
        filter_tab_placeholder_text = filter_tab_placeholder.text()

    $('.cd-tab-filter li').on('click', function (event) {
        //detect which tab filter item was selected
        var selected_filter = $(event.target).data('type')

        //check if user has clicked the placeholder item
        if ($(event.target).is(filter_tab_placeholder)) {
            filter_tab_placeholder_default_value == filter_tab_placeholder.text()
                ? filter_tab_placeholder.text(filter_tab_placeholder_text)
                : filter_tab_placeholder.text(filter_tab_placeholder_default_value)
            $('.cd-tab-filter').toggleClass('is-open')

            //check if user has clicked a filter already selected
        } else if (filter_tab_placeholder.data('type') == selected_filter) {
            filter_tab_placeholder.text($(event.target).text())
            $('.cd-tab-filter').removeClass('is-open')
        } else {
            //close the dropdown and change placeholder text/data-type value
            $('.cd-tab-filter').removeClass('is-open')
            filter_tab_placeholder.text($(event.target).text()).data('type', selected_filter)
            filter_tab_placeholder_text = $(event.target).text()

            //add class selected to the selected filter item
            $('.cd-tab-filter .selected').removeClass('selected')
            $(event.target).addClass('selected')
        }
    })

    //close filter dropdown inside lateral .cd-filter
    $('.cd-filter-block h4').on('click', function () {
        $(this).toggleClass('closed').siblings('.cd-filter-content').slideToggle(300)
    })

    //fix lateral filter and gallery on scrolling
    $(window).on('scroll', function () {
        !window.requestAnimationFrame ? fixGallery() : window.requestAnimationFrame(fixGallery)
    })

    function fixGallery() {
        var offsetTop = $('.cd-main-content').offset().top,
            scrollTop = $(window).scrollTop()
        scrollTop >= offsetTop
            ? $('.cd-main-content').addClass('is-fixed')
            : $('.cd-main-content').removeClass('is-fixed')
    }

    /************************************
		MitItUp filter settings
		More details: 
		https://mixitup.kunkalabs.com/
		or:
		http://codepen.io/patrickkunka/
	*************************************/

    buttonFilter.init()
    $('.cd-gallery ul').mixItUp({
        controls: {
            enable: false,
        },
        callbacks: {
            onMixStart: function () {
                $('.cd-fail-message').fadeOut(200)
            },
            onMixFail: function () {
                $('.cd-fail-message').fadeIn(200)
            },
        },
    })

    //search filtering
    //credits http://codepen.io/edprats/pen/pzAdg
    var inputText
    var $matching = $()

    var delay = (function () {
        var timer = 0
        return function (callback, ms) {
            clearTimeout(timer)
            timer = setTimeout(callback, ms)
        }
    })()

    $(".cd-filter-content input[type='search']").keyup(function () {
        // Delay function invoked to make sure user stopped typing
        delay(function () {
            inputText = $(".cd-filter-content input[type='search']").val().toLowerCase()
            // Check to see if input field is empty
            if (inputText.length > 0) {
                $('.mix').each(function () {
                    var $this = $(this)

                    // add item to be filtered out if input text matches items inside the title
                    if ($this.attr('class').toLowerCase().match(inputText)) {
                        $matching = $matching.add(this)
                    } else {
                        // removes any previously matched item
                        $matching = $matching.not(this)
                    }
                })
                $('.cd-gallery ul').mixItUp('filter', $matching)
            } else {
                // resets the filter to show all item if input is empty
                $('.cd-gallery ul').mixItUp('filter', 'all')
            }
        }, 200)
    })
})

/*****************************************************
	MixItUp - Define a single object literal 
	to contain all filter custom functionality
*****************************************************/
var buttonFilter = {
    // Declare any variables we will need as properties of the object
    $filters: null,
    groups: [],
    outputArray: [],
    outputString: '',

    // The "init" method will run on document ready and cache any jQuery objects we will need.
    init: function () {
        var self = this // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "buttonFilter" object so that we can share methods and properties between all parts of the object.

        self.$filters = $('.cd-main-content')
        self.$container = $('.cd-gallery ul')

        self.$filters.find('.cd-filters').each(function () {
            var $this = $(this)

            self.groups.push({
                $inputs: $this.find('.filter'),
                active: '',
                tracker: false,
            })
        })

        self.bindHandlers()
    },

    // The "bindHandlers" method will listen for whenever a button is clicked.
    bindHandlers: function () {
        var self = this

        self.$filters.on('click', 'a', function (e) {
            self.parseFilters()
        })
        self.$filters.on('change', function () {
            self.parseFilters()
        })
    },

    parseFilters: function () {
        var self = this

        // loop through each filter group and grap the active filter from each one.
        for (var i = 0, group; (group = self.groups[i]); i++) {
            group.active = []
            group.$inputs.each(function () {
                var $this = $(this)
                if ($this.is('input[type="radio"]') || $this.is('input[type="checkbox"]')) {
                    if ($this.is(':checked')) {
                        group.active.push($this.attr('data-filter'))
                    }
                } else if ($this.is('select')) {
                    group.active.push($this.val())
                } else if ($this.find('.selected').length > 0) {
                    group.active.push($this.attr('data-filter'))
                }
            })
        }
        self.concatenate()
    },

    concatenate: function () {
        var self = this

        self.outputString = `` // Reset output string

        for (var i = 0, group; (group = self.groups[i]); i++) {
            self.outputString += group.active
        }
        console.log(self.outputString.length)
        // If the output string is empty, show all rather than none:
        !self.outputString.length && (self.outputString = 'all')

        // Send the output string to MixItUp via the 'filter' method:
        if (self.$container.mixItUp('isLoaded')) {
            self.$container.mixItUp('filter', self.outputString)
        }
    },
}
//---- below is mine program

//<a href="http://${currList.product_url}" target="_blank">color-1 radio2 option3
