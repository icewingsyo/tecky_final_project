import * as Knex from 'knex';
import { tables } from '../utils/tables';

const usersTable = tables.USERS;
const customerTable = tables.CUSTOMER;
const sellerTable = tables.SELLER;
const brandTable = tables.BRAND;
const productTable = tables.PRODUCT;
const customer_sessionTable = tables.CUSTOMER_SESSION;
const categoryTable = tables.CATEGORY;
const sub_categoryTable = tables.SUB_CATEGORY;
const colorTable = tables.COLOR;
const priceTable = tables.PRICE;
const styleTable = tables.STYLE;
const ratingTable = tables.RATING;
const like_stringTable = tables.LIKE_STRING;
const like_productTable = tables.LIKE_PRODUCT;
const like_brandTable = tables.LIKE_BRAND;
const photoTable = tables.PHOTO;
const facebookTable = tables.FACEBOOK_LOGIN;
const googleTable = tables.GOOGLE_LOGIN;
const appleTable = tables.APPLE_LOGIN;

export async function up(knex: Knex): Promise<void> {
  // user
  let hasTable =await knex.schema.hasTable(usersTable);
  if(!hasTable){
    await knex.schema.createTable(usersTable, (table)=>{
      table.increments();
      table.boolean('seller_is_active').notNullable().defaultTo(false);
      table.boolean('users_is_active').notNullable().defaultTo(true);
      table.timestamps(false,true);
    });
  }//-----------------------
  // brand
  hasTable = await knex.schema.hasTable(brandTable);
  if (!hasTable) {
    await knex.schema.createTable(brandTable, (table) => {
      table.increments();
      table.string('brand_name').notNullable();
      table.string('brand_pic');
      table.boolean('is_active').defaultTo(true).notNullable();
      table.timestamps(false, true);
    });
  } //------------------
  // photo
  hasTable = await knex.schema.hasTable(photoTable);
  if (!hasTable) {
    await knex.schema.createTable(photoTable, (table) => {
      table.increments();
      table.string('photo_str', 255);
      table.timestamps(false, true);
    });
  } //------------------
  // facebook_login
  hasTable = await knex.schema.hasTable(facebookTable);
  if (!hasTable) {
    await knex.schema.createTable(facebookTable, (table) => {
      table.increments();
      table.string('auth_code', 255);
      table.timestamps(false, true);
    });
  } //------------------
  // google_login
  hasTable = await knex.schema.hasTable(googleTable);
  if (!hasTable) {
    await knex.schema.createTable(googleTable, (table) => {
      table.increments();
      table.string('auth_code', 255);
      table.timestamps(false, true);
    });
  } //------------------
  // apple_login
  hasTable = await knex.schema.hasTable(appleTable);
  if (!hasTable) {
    await knex.schema.createTable(appleTable, (table) => {
      table.increments();
      table.string('auth_code', 255);
      table.timestamps(false, true);
    });
  } //------------------
  // category
  hasTable = await knex.schema.hasTable(categoryTable);
  if (!hasTable) {
    await knex.schema.createTable(categoryTable, (table) => {
      table.increments();
      table.string('category_name', 255).notNullable();
      table.timestamps(false, true);
    });
  } //------------------
  // sub_category
  hasTable = await knex.schema.hasTable(sub_categoryTable);
  if (!hasTable) {
    await knex.schema.createTable(sub_categoryTable, (table) => {
      table.increments();
      table.string('sub_category_name', 255).notNullable();
      table.timestamps(false, true);
    });
  } //------------------
  // customer
  hasTable = await knex.schema.hasTable(customerTable);
  if (!hasTable) {
    await knex.schema.createTable(customerTable, (table) => {
      table.increments();
      table.integer('users_id').notNullable().unique().unsigned();
      table.foreign('users_id').references(`${usersTable}.id`)
      table.string('customer_first_name');
      table.string('customer_last_name');
      table.text('password').notNullable();
      table.string('username');
      table.string('email').notNullable().unique();
      table.text('location');
      table.string('gender', 6).defaultTo('secret');
      table.text('preferred_category');
      table.integer('photo_id');
      table.foreign('photo_id').references(`${photoTable}.id`);
      table.integer('facebook_login_id');
      table.foreign('facebook_login_id').references(`${facebookTable}.id`);
      table.integer('google_login_id');
      table.foreign('google_login_id').references(`${googleTable}.id`);
      table.integer('apple_login_id');
      table.foreign('apple_login_id').references(`${appleTable}.id`);
      table.boolean('is_active').defaultTo(true);
      table.timestamps(false, true);
    });
  } //------------------
  // seller
  hasTable = await knex.schema.hasTable(sellerTable);
  if(!hasTable){
    await knex.schema.createTable(sellerTable, (table)=>{
      table.increments();
      table.integer('users_id').notNullable().unique().unsigned();
      table.foreign('users_id').references(`${sellerTable}.id`);
      table.text('pending').notNullable();
      table.integer('brand_id').notNullable().unique().unsigned();
      table.foreign('brand_id').references(`${brandTable}.id`);
      table.timestamps(false,true);
    })
  }//-------------------
  // product
  hasTable = await knex.schema.hasTable(productTable);
  if (!hasTable) {
    await knex.schema.createTable(productTable, (table) => {
      table.increments();
      table.integer('product_id_code').notNullable().unsigned().unique();
      table.text('product_name').notNullable();
      table.integer('brand_id').notNullable().unsigned();
      table.foreign('brand_id').references(`${brandTable}.id`);
      table.string('product_url').notNullable();
      table.text('product_pic');
      table.integer('subcategory_id').notNullable().unsigned();
      table.foreign('subcategory_id').references(`${sub_categoryTable}.id`);
      table.integer('category_id').notNullable().unsigned();
      table.foreign('category_id').references(`${categoryTable}.id`);
      table.string('gender').notNullable();
      table.boolean('is_active').defaultTo(true).notNullable();
      table.timestamps(false, true);
    });
  } //------------------
  // like_string
  hasTable = await knex.schema.hasTable(like_stringTable);
  if (!hasTable) {
    await knex.schema.createTable(like_stringTable, (table) => {
      table.increments();
      table.integer('users_id').notNullable().unsigned();
      table.foreign('users_id').references(`${usersTable}.id`);
      table.text('string_favourite').nullable();
      table.timestamps(false, true);
    });
  } //------------------
  // like_brand
  hasTable = await knex.schema.hasTable(like_brandTable);
  if (!hasTable) {
    await knex.schema.createTable(like_brandTable, (table) => {
      table.increments();
      table.integer('users_id').notNullable().unsigned();
      table.foreign('users_id').references(`${usersTable}.id`);
      table.integer('brand_id').notNullable().unsigned();
      table.foreign('brand_id').references(`${brandTable}.id`);
      table.timestamps(false, true);
    });
  } //------------------
  // like_product
  hasTable = await knex.schema.hasTable(like_productTable);
  if (!hasTable) {
    await knex.schema.createTable(like_productTable, (table) => {
      table.increments();
      table.integer('users_id').notNullable().unsigned();
      table.foreign('users_id').references(`${usersTable}.id`);
      table.integer('product_id').notNullable().unsigned();
      table.foreign('product_id').references(`${productTable}.id`);
      table.timestamps(false, true);
    });
  } //------------------
  // price
  hasTable = await knex.schema.hasTable(priceTable);
  if (!hasTable) {
    await knex.schema.createTable(priceTable, (table) => {
      table.increments();
      table.double('discount_price').unsigned();
      table.double('original_price').notNullable().unsigned();
      table.integer('product_id').notNullable().unsigned();
      table.foreign('product_id').references(`${productTable}.id`);
      table.timestamps(false, true);
    });
  }//-------------------
  // style
  hasTable = await knex.schema.hasTable(styleTable);
  if (!hasTable) {
    await knex.schema.createTable(styleTable, (table) => {
      table.increments();
      table.string('style_name');
      table.integer('product_id').notNullable().unsigned();
      table.foreign('product_id').references(`${productTable}.id`);
      table.timestamps(false, true);
    });
  }//-------------------
  // color
  hasTable = await knex.schema.hasTable(colorTable);
  if (!hasTable) {
    await knex.schema.createTable(colorTable, (table) => {
      table.increments();
      table.string('color_name');
      table.integer('product_id').notNullable().unsigned();
      table.foreign('product_id').references(`${productTable}.id`);
      table.timestamps(false, true);
    });
  }//-------------------
  // customer_prefer
  hasTable = await knex.schema.hasTable(customer_sessionTable);
  if (!hasTable) {
    await knex.schema.createTable(customer_sessionTable, (table) => {
      table.increments();
      table.integer('customer_id').notNullable().unsigned();
      table.foreign('customer_id').references(`${customerTable}.id`);
      table.integer('product_id').notNullable().unsigned();
      table.foreign('product_id').references(`${productTable}.id`);
      table.string('session_code');
      table.string('event').notNullable().comment('click, view, buy');
      table.timestamps(false, true);
    });
  } //------------------
  //rating
  hasTable = await knex.schema.hasTable(ratingTable);
  if (!hasTable) {
    await knex.schema.createTable(ratingTable, (table) => {
      table.increments();
      table.integer('customer_id').notNullable().unsigned();
      table.foreign('customer_id').references(`${customerTable}.id`);
      table.integer('product_id').notNullable().unsigned();
      table.foreign('product_id').references(`${productTable}.id`);
      table.double('rating').notNullable();
      table.timestamps(false, true);
    });
  }//-------------------
} //-------------------

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists(ratingTable);
  await knex.schema.dropTableIfExists(customer_sessionTable);
  await knex.schema.dropTableIfExists(colorTable);
  await knex.schema.dropTableIfExists(styleTable);
  await knex.schema.dropTableIfExists(priceTable);
  await knex.schema.dropTableIfExists(like_productTable);
  await knex.schema.dropTableIfExists(like_brandTable);
  await knex.schema.dropTableIfExists(like_stringTable);
  await knex.schema.dropTableIfExists(productTable);
  await knex.schema.dropTableIfExists(sellerTable);
  await knex.schema.dropTableIfExists(customerTable);
  await knex.schema.dropTableIfExists(sub_categoryTable);
  await knex.schema.dropTableIfExists(categoryTable);
  await knex.schema.dropTableIfExists(appleTable);
  await knex.schema.dropTableIfExists(googleTable);
  await knex.schema.dropTableIfExists(facebookTable);
  await knex.schema.dropTableIfExists(photoTable);
  await knex.schema.dropTableIfExists(brandTable);
  await knex.schema.dropTableIfExists(usersTable);
}
