import React, { useEffect } from 'react';
import { Container } from 'reactstrap';
import modules from '../css/FriendList.module.css';
import FriendCard from '../components/FriendCard';
import {IRootState} from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getFriendListThunk } from '../redux/friends/thunk';
import { Pagination } from 'react-bootstrap';

function InfListTest() {
    const [page, setPage] = userState(1);
    const friendList = useSelector((state:IRootState) => state.products.productList);
    const activePage = useSelector((state:IRootState) => state.products.activePage);
    const nextPage = useSelector((state:IRootState) => state.products.nextPage);
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(getFriendListThunk(1, true));
    }, [dispatch]);

    const selectPageHandler = (pageNum:number) =>{
        dispatch(getFriendListThunk(pageNum, true));
    }
    console.log(friendList);
    const onScroll=((e:any) =>{
        console.log('Top '+e.currentTarget.scrollTop, 'Height '+e.currentTarget.scrollHeight);
    })
    return (
        <>
        <Container>
            <h1>This is friend list page</h1>
            { friendList && friendList.length > 0 && (
                <div className={modules.friendlist} onScroll={onScroll}>
                {/*
                    friendList.map((friend)=>(
                        <FriendCard 
                            key={`friend_${friend.id}`} 
                            firstName={friend.first_name} 
                            lastName={friend.last_name} 
                            email={friend.email} 
                            avatar={friend.avatar}
                            userId={friend.id} 
                        />
                    ))
                    */}
                </div> 
                
            )}
            <Pagination>
                {Array(nextPage).fill(null).map((_, idx)=>{
                    return <Pagination.Item key={`page_${idx + 1}`} active={idx + 1 === activePage} onClick={()=>selectPageHandler(idx + 1)}>
                        {idx + 1}
                    </Pagination.Item>
                })}
            </Pagination>
        </Container>
        </>
    )
}

export default InfListTest
function userState(arg0: number): [any, any] {
    throw new Error('Function not implemented.');
}

