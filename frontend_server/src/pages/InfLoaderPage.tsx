import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import FriendCard from '../components/FriendCard';
import { IRootState } from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getFriendListThunk } from "../redux/friends/thunk";
import modules from '../css/FriendList.module.css';

function InfLoaderPage() {
  const dispatch = useDispatch();
  const friendList = useSelector((state:IRootState) => state.products.productList);
  const activePage = useSelector((state:IRootState) => state.products.activePage);
  const nextPage = useSelector((state:IRootState) => state.products.nextPage);
  const limit = useSelector((state:IRootState) => state.products.limit);
  const [pageCount, setPageCount] = useState(activePage);
  // page setPage
  useEffect(() => {
      dispatch(getFriendListThunk(1, true));
  }, [dispatch]);
  useEffect(() =>{
    setPageCount(nextPage);
  }, [activePage])
  console.log(friendList);
  console.log(limit);
  console.log(`active page ${activePage}`);
  console.log(`page count ${pageCount}`);
  console.log(`next page ${nextPage}`);
  const [items, setItems] = useState(Array(10).fill(friendList)); // setItem in loadFunc
  const loadFunc = () => {  
    if(friendList && items.length > 0 && friendList.length > 0){
      console.log("test loadFunc");
      dispatch(getFriendListThunk(pageCount , false));
      const newItems = Array(10).fill(friendList);
      const toInsertItems = items.concat(newItems);
      setItems(toInsertItems);
    }
  };
  
  return (
    <>
      <h1>Inf Loader Demo</h1>
      
        <InfiniteScroll
          pageStart={0}
          loadMore={loadFunc}
          hasMore={true || false}
          loader={
            <div className="loader" key={0}>
              Loading ...
            </div>
          }
        >
          <div className="infFlexCol">
          { items && items.length > 0 && (
                <div className={modules.friendlist}>
                
                </div> 
                
            )}
          </div>
        </InfiniteScroll>
      
    </>
  );
}

export default InfLoaderPage;