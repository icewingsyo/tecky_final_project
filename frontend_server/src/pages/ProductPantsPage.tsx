import React, { useEffect, useState } from 'react';
import { Button, Container } from 'reactstrap';
import modules from '../css/FriendList.module.css';
import FriendCard from '../components/FriendCard';
import {IRootState} from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getProductListByClassThunk } from '../redux/friends/thunk';
import ProductSwitch from '../components/ProductSwitch';
import GoToTopButton from '../components/GoToTopButton';

function ProductPantsPage() {
    const productList = useSelector((state:IRootState) => state.products.productList);
    const limit = useSelector((state:IRootState)=> state.products.limit);
    const dispatch = useDispatch();
    const [page, setPage] = useState(2);
    
    useEffect(() => {
        dispatch(getProductListByClassThunk(1, 'Pants', true));
    }, [dispatch]);

    const loadPageHandler = (pageNum:number) =>{
        setPage(pageNum + 1);
        dispatch(getProductListByClassThunk(page, 'Pants', false));
    }
    return (
        <>
        <Container>
            <ProductSwitch />
            <h1>Products/Pants</h1>
            { productList && productList.length > 0 && (
                <div className={modules.friendlist}>
                {
                    productList.map((product, idx)=>(
                        <FriendCard 
                            key={`product_${idx + 1}`} 
                            id={product.id}
                            product_id_code={product.product_id_code}
                            product_name={product.product_name}
                            brand_name={product.brand_name}
                            product_url={product.product_url}
                            product_pic={product.product_pic}
                            sub_category_name={product.sub_category_name}
                            category_name={product.category_name}
                            gender={product.gender}
                            original_price={product.original_price}
                            color_name={product.color_name}
                        />
                    ))
                }
                </div> 
                
            )}
            {productList && productList.length > 0 && limit === false && (
                <div className={modules.loadMore}>
                    <Button color="primary" light onClick={()=>{loadPageHandler(page)}}>Load More</Button>
                </div>
                )}
            {limit === true && (
                <div className={modules.loadMore}><h4>No more products</h4></div>
            )}
        </Container>
        <div>
            <GoToTopButton />  
        </div>
    </>
    )
}

export default ProductPantsPage;
