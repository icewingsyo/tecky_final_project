import React from 'react';
import { useForm, Controller } from "react-hook-form";
import modules from '../css/RegisterPage.module.css';
import { Button, Form, Container } from 'react-bootstrap';
import {registerThunk} from '../redux/auth/thunk';
import { useDispatch } from 'react-redux';


export interface IRegData {
    customer_first_name: string,
    customer_last_name: string,
    password: string,
    username: string,
    email: string,
    location: string,
    gender: string,
    preferredCategory: string
}

function RegisterPage() {
    const dispatch = useDispatch();
    const { control, handleSubmit, formState: {errors} } = useForm<IRegData>();
    const onSubmit = (data: IRegData) => {
        console.log(data);
        dispatch(registerThunk(data));
    };
    
    return (
        <div>
            <h1>Register page</h1>
            <Container className={modules.container}>
                <Form onSubmit={handleSubmit(onSubmit)}>
                    
                        <Form.Group controlId="formFirstName">
                        <Form.Label>First Name | Last Name</Form.Label>
                                    <Controller
                                    name="customer_first_name"
                                    control={control}
                                    defaultValue=""
                                    rules={{ required: true }}
                                    render={({ field }) => <Form.Control {...field} 
                                    placeholder="Enter Your first name" required/>}
                                    />
                                
                            </Form.Group>
                            <Form.Group controlId="formLastName">
                                    <Controller
                                    name="customer_last_name"
                                    control={control}
                                    defaultValue=""
                                    rules={{ required: true }}
                                    render={({ field }) => <Form.Control {...field} 
                                    placeholder="Enter Your last name" required/>}
                                    />
                            </Form.Group>
  
                    <Form.Group controlId="formRegPassword">
                        <Form.Label>Password</Form.Label>
                        <Controller
                            name="password"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                            
                            render={({ field }) => <Form.Control {...field} 
                            placeholder="Enter Your Password" type="password" required />}
                            />                    
                    </Form.Group>

                    <Form.Group controlId="formUsername">
                        <Form.Label>Username</Form.Label>
                        <Controller
                            name="username"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                            render={({ field }) => <Form.Control {...field} 
                            placeholder="Username show in website" required/>}
                            />
                    </Form.Group>

                    <Form.Group controlId="formRegEmail">
                        <Form.Label>E-mail</Form.Label>
                        <Controller
                            name="email"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                            render={({ field }) => <Form.Control {...field} 
                            placeholder="Enter Your E-mail" type="email" required/>}
                            />
                    </Form.Group>

                    <Form.Group controlId="formLocation">
                        <Form.Label>Location</Form.Label>
                        <Controller
                            name="location"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                            render={({ field }) => <Form.Control {...field} 
                            placeholder="Enter Your Location" />}
                            />
                    </Form.Group>

                    <Form.Group controlId="formGender">
                        <Form.Label>Gender</Form.Label>
                        <Controller
                            name="gender"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                            render={({ field }) => <Form.Control {...field} as="select"
                            defaultValue="secert" required>
                                <option>secert</option>
                                <option>M</option>
                                <option>F</option>
                            </Form.Control>}
                            />
                    </Form.Group>

                    <Form.Group controlId="formPreferredCategory">
                        <Form.Label>Preferred Category</Form.Label>
                        <Controller
                            name="preferredCategory"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                            render={({ field }) => <Form.Control {...field} as="select"
                            defaultValue="all" required>
                                <option>all</option>
                                <option>pants</option>
                                <option>bag</option>
                                <option>hat</option>
                                <option>socks</option>
                                <option>clothing</option>
                                <option>accessories</option>
                                <option>skirt</option>
                                <option>shoes</option>
                                <option>shawls</option>
                                <option>others</option>
                            </Form.Control>}
                            />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Register
                    </Button>
                </Form>
            </Container>
            {errors && errors.email}
        </div>
    )
}

export default RegisterPage;
