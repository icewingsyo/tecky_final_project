import React, { useEffect } from 'react';
import { Container } from 'reactstrap';
import modules from '../css/FriendList.module.css';
import {IRootState} from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getFriendListThunk } from '../redux/friends/thunk';
import { Pagination } from 'react-bootstrap';

function FriendListPage() {
    const friendList = useSelector((state:IRootState) => state.products.productList);
    const activePage = useSelector((state:IRootState) => state.products.activePage);
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(getFriendListThunk(1, true));
    }, [dispatch]);

    const selectPageHandler = (pageNum:number) =>{
        dispatch(getFriendListThunk(pageNum, true));
    }
    console.log(friendList);
    return (
        <>
        <Container>
            <h1>This is friend list page</h1>
            { friendList && friendList.length > 0 && (
                <div className={modules.friendlist}>
                
                </div> 
                
            )}
            <Pagination>
                {Array(10).fill(null).map((_, idx)=>{
                    return <Pagination.Item key={`page_${idx + 1}`} active={idx + 1 === activePage} onClick={()=>selectPageHandler(idx + 1)}>
                        {idx + 1}
                    </Pagination.Item>
                })}
            </Pagination>
        </Container>
        </>
    )
}

export default FriendListPage
