import { push } from 'connected-react-router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Container } from 'react-bootstrap';
import modules from '../css/LoginPage.module.css';
import { useForm, Controller } from "react-hook-form";
import { loginThunk, restoreLoginThunk } from '../redux/auth/thunk';
import { IRootState } from '../redux/store';
import { Label } from 'reactstrap';

interface IInputData{
    email: string;
    password: string;
}

function LoginPage() {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector((state: IRootState)=> state.auth.isAuthenticated);
    useEffect(() =>{
        if(isAuthenticated){
          dispatch(restoreLoginThunk());
          dispatch(push('/home'));
        }
      },[dispatch, isAuthenticated]);

    const { control, handleSubmit, formState: {errors} } = useForm<IInputData>();
    const onSubmit = (data: IInputData) => {
        dispatch(loginThunk(data.email, data.password));
        console.log(data);
        console.log(isAuthenticated);
    };
    
    return (
        <Container className={modules.container}>
            <h1>Login page</h1>
            <div>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="formLoginEmail">
                    <Form.Label>Email</Form.Label>
                    <>
                    <Controller
                        name="email"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                        render={({ field }) => <Form.Control {...field} 
                        placeholder="Enter Your E-mail" />}
                        />
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    
                    </>

                </Form.Group>

                <Form.Group controlId="formLoginPassword">
                    <Form.Label>Password</Form.Label>
                    <Controller
                        name="password"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                        render={({ field }) => <Form.Control {...field} 
                        placeholder="Enter Your Password" type="password" required/>}
                        />                    
                </Form.Group>
                
                <Button variant="primary" type="submit">
                    Login
                </Button>
                <div>
                    <Label className="text-danger">{errors.email && <span>Username/Password has wrong.</span> && errors.password && <span>Username/Password has wrong.</span>}</Label>
                </div>
            </Form>
            
            </div>
            
        </Container>
    )
}

export default LoginPage;
