import { push } from 'connected-react-router';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { restoreLoginThunk } from '../redux/auth/thunk';
import { IRootState } from '../redux/store';
import GroupButtonHome from '../components/GroupButtonHome';
import Logo from '../components/Logo';


function LandingPage() {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector((state: IRootState)=> state.auth.isAuthenticated);
    useEffect(() =>{
        if(isAuthenticated === null){
            dispatch(restoreLoginThunk());
        }
        if(isAuthenticated === true){
            dispatch(push("/home"));
        }
    },[dispatch, isAuthenticated]);
    return (
        <>
            <Logo />
            <GroupButtonHome />
        </>
    )
}

export default LandingPage;
