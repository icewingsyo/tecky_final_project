import { CallHistoryMethodAction } from "connected-react-router";
import { IProduct } from "./state";

export function setProductList(products: Array<IProduct>, activePage: number, nextPage: number, limit: boolean){
    return{
        type: "@@FRIENDS/SET_FRIEND_LIST" as const,
        products,
        activePage,
        nextPage,
        limit
    };
}

export function resetProductList(){
    return{
        type: "@@FRIENDS/RESET" as const,
    };
}

type ProductActionCreators = typeof setProductList | typeof resetProductList;

export type IProductActions = ReturnType<ProductActionCreators> | CallHistoryMethodAction;
