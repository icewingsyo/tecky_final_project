import { ConnectedRouter } from 'connected-react-router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import LandingPage from './pages/LandingPage';
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import UserProfile from './pages/UserProfile';
import { IRootState, history } from './redux/store';
import PrivateRoute from './components/PrivateRoute';
import { restoreLoginThunk } from './redux/auth/thunk';
import Loading from './components/Loading';
import LoginModalBody from './components/LoginModalBody';
import NavBarComp from './components/NavBarComp';
import FriendListPage from './pages/FriendListPage';
import ProductAllPage from './pages/ProductAllPage';
import ProductPantsPage from './pages/ProductPantsPage';
import ProductBagPage from './pages/ProductBagPage';
import ProductHatPage from './pages/ProductHatPage';
import ProductSocksPage from './pages/ProductSocksPage';
import ProductClothingPage from './pages/ProductClothingPage';
import ProductAccessoriesPage from './pages/ProductAccessoriesPage';
import ProductSkirtPage from './pages/ProductSkirtPage';
import ProductShoesPage from './pages/ProductShoesPage';
import ProductOthersPage from './pages/ProductOthersPage';
import ProductShawlsPage from './pages/ProductShawlsPage';

function App() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state: IRootState)=> state.auth.isAuthenticated);
  useEffect(() =>{
    if(isAuthenticated === null){
      dispatch(restoreLoginThunk());
    }
  },[dispatch, isAuthenticated]);

  if(isAuthenticated === null){
    return <Loading />
  }
  return (
    <div className="App">
      <ConnectedRouter history={history}>
        <div className="fixOnPage"><NavBarComp /></div>
        <Switch>
          <Route path="/" exact={true} component={LandingPage}  />
          <Route path="/loginmodal" exact={true} component={LoginPage}  />
          <Route path="/login" exact={true} component={LoginModalBody}  />
          <Route path="/product/All" exact={true} component={ProductAllPage}  />
          <Route path="/product/Pants" exact={true} component={ProductPantsPage}  />
          <Route path="/product/Bag" exact={true} component={ProductBagPage}  />
          <Route path="/product/Hat" exact={true} component={ProductHatPage}  />
          <Route path="/product/Socks" exact={true} component={ProductSocksPage}  />
          <Route path="/product/Clothing" exact={true} component={ProductClothingPage}  />
          <Route path="/product/Accessories" exact={true} component={ProductAccessoriesPage}  />
          <Route path="/product/Skirt" exact={true} component={ProductSkirtPage}  />
          <Route path="/product/Shoes" exact={true} component={ProductShoesPage}  />
          <Route path="/product/Shawls" exact={true} component={ProductShawlsPage}  />
          <Route path="/product/Others" exact={true} component={ProductOthersPage}  />
          <PrivateRoute path="/home" exact={true} component={HomePage}  />
          <PrivateRoute path="/userprofile" exact={true} component={UserProfile}  />
          <PrivateRoute path="/friendlist" exact={true} component={FriendListPage} />
          <PrivateRoute path="/product/login/all" exact={true} component={ProductAllPage} />
        </Switch>
        </ConnectedRouter>
    </div>
  );
}

export default App;
