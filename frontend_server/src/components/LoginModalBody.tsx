import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Label } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import LoginModal from '../components/LoginModal';
import RegisterModal from '../components/RegisterModal';

function LoginModalBody(props:any) {
    const {
        className
      } = props;
    
      const [modal, setModal] = useState(false);
    
      const toggle = () => setModal(!modal);
    
      const closeBtn = <button className="close" onClick={toggle}>&times;</button>;

      const [activeTab, setActiveTab] = useState('1');

        const tabToggle = (tab:string) => {
            if(activeTab !== tab) setActiveTab(tab);
        }
    return (
        <div>
            <NavLink onClick={toggle}><Label>Login</Label></NavLink>
            <Modal isOpen={modal} toggle={toggle} className={className} style={{top:'100px'}}>
            <ModalHeader toggle={toggle} close={closeBtn}></ModalHeader>
            <ModalBody>
            <Nav tabs>
                <Row>
                    <Col>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: activeTab === '1' })}
                                onClick={() => { tabToggle('1'); }}
                            >
                                <Label>Login</Label>
                            </NavLink>
                        </NavItem>
                    </Col>
                    <Col>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: activeTab === '2' })}
                                onClick={() => { tabToggle('2'); }}
                            >
                                <Label>Register</Label>
                            </NavLink>
                        </NavItem>
                    </Col>
                </Row>
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                <Row>
                    <Col sm="12">
                        <LoginModal />
                    </Col>
                </Row>
                </TabPane>
                <TabPane tabId="2">
                <Row>
                    <Col sm="12">
                        <RegisterModal />
                    </Col>
                </Row>
                </TabPane>
            </TabContent>
            </ModalBody>
            <ModalFooter>
            </ModalFooter>
            </Modal>
            
        </div>
    )
}

export default LoginModalBody;
