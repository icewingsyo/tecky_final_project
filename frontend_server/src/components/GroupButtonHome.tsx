import { push } from 'connected-react-router';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'reactstrap';
import modules from  '../css/GroupButtonHome.module.css';

function GroupButtonHome() {
    const dispatch = useDispatch();
    return (
        <div className={modules.button}>
            <div>
                <div>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/all'))}>All</Button>
                </div>
                <div>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Pants'))}>Pants</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Bag'))}>Bag</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Hat'))}>Hat</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Socks'))}>Socks</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Clothing'))}>Clothing</Button>
                </div>
                <div>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Accessories'))}>Accessories</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Skirt'))}>Skirt</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Shoes'))}>Shoes</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Shawls'))}>Shawls</Button>
                    <Button variant="outline-dark" onClick={()=>dispatch(push('/product/Others'))}>Others</Button>
                </div> 
            </div>
            
        </div>
    )
}

export default GroupButtonHome;
