import React from 'react';
import { Breadcrumb } from 'react-bootstrap';


function ProductSwitch() {
    return (
        <div>
            <Breadcrumb>
                <Breadcrumb.Item href="/product/All">All</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Pants">Pants</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Bag">Bag</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Hat">Hat</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Socks">Socks</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Clothing">Clothing</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Accessories">Accessories</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Skirt">Skirt</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Shoes">Shoes</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Shawls">Shawls</Breadcrumb.Item>
                <Breadcrumb.Item href="/product/Others">Others</Breadcrumb.Item>
            </Breadcrumb>
        </div>
    )
}

export default ProductSwitch;
