import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import logo from '../img/logo.png';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import { logout } from '../redux/auth/action';

function NavBarCompLogin() {
    const [collapsed, setCollapsed] = useState(true);
    const toggleNavbar = () => setCollapsed(!collapsed); // default false
    const dispatch = useDispatch();

    const profileClick = () => dispatch(push('/userprofile'));
    const logoutClick = () => {
        dispatch(logout());
        localStorage.removeItem("token");
        dispatch(push("/"));
    }
    
    return (
            <Navbar color="faded" light>
                <NavbarBrand href="/home" className="mr-auto"><img src={logo} alt='logo' /></NavbarBrand>
                <NavbarToggler onClick={toggleNavbar} className="mr-2" />
                <Collapse isOpen={!collapsed} navbar>
                <Nav navbar>
                    <NavItem>
                       <NavLink onClick={profileClick}>Profile</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="mailto:contact-us@shopround.co">Contact us</NavLink>
                    </NavItem>
                    <NavItem>
                    <NavLink onClick={logoutClick}>Logout</NavLink>
                    </NavItem>
                </Nav>
                </Collapse>
            </Navbar>
    )
}

export default NavBarCompLogin;
