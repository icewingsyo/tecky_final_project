import React from 'react';
import { Spinner } from 'react-bootstrap';
import modules from '../css/Loading.module.css';

function Loading() {

    return (
        <div className={modules.container}>
            <Spinner animation="grow" className={modules.spinner} />
            <h1>Loading...</h1>
        </div>
    )
}


export default Loading
