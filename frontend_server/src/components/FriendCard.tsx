import React, { useState } from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Label
  } from 'reactstrap';
import modules from '../css/FriendCard.module.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'; 
import Masonry from "react-masonry-css";

interface IFriendCardProps{
    id: number;
    product_id_code: number;
    product_name: string;
    brand_name: number;
    product_url: string;
    product_pic: string;
    sub_category_name: number;
    category_name: number;
    gender: string;
    original_price: number;
    color_name: string;
}

function FriendCard({ id, product_id_code, product_name, brand_name, product_url, product_pic, sub_category_name, category_name, gender, original_price, color_name}:IFriendCardProps) {
    
    const BREAKPOINT_COLS = { default: 5, 1100: 4, 700: 3, 500:2,};

    const className = `User_${id}`;
    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);
    let splitPic = product_pic.split(',')[0];
    let sliceName = product_name.slice(0, 18);

    return (
        <>
            <Masonry breakpointCols={BREAKPOINT_COLS} className="my-masonry-grid" columnClassName="my-masonry-grid_column">
                <Card className={modules.friendCard} onClick={toggle}>
                    <CardImg variant="top" src={splitPic} alt={`Card image cap ${product_id_code}`} className={modules.listImage}/>
                    <CardBody>
                    <CardTitle tag="h6">{sliceName}{'...'}</CardTitle>
                    <CardSubtitle tag="h6" className="mb-2 text-muted">{brand_name}</CardSubtitle>
                    <CardText>{'HKD$'}{original_price}</CardText>
                    </CardBody>
                </Card>
            </Masonry>
            <Modal isOpen={modal} toggle={toggle} className={className} style={{ top:'200px'}}>
                <ModalHeader toggle={toggle}>{product_name}</ModalHeader>
                <ModalBody>
                    <div className="product_details_block">
                        <div className="product_img">
                            <img src={splitPic} alt={product_name} className={modules.detailImage}/>
                        </div>
                        <div className="product_details">
                            <div>
                                <Label>Product Name:</Label><br />
                                <Label>Category:</Label><br />
                                <Label>Gender:</Label><br />
                                <Label>Color Option:</Label><br />
                                <Label>Price:</Label><br />
                            </div>
                            <div>
                                <Label>{product_name}</Label><br />
                                <Label>{category_name}{'/'}{sub_category_name}</Label><br />
                                <Label>{gender}</Label><br />
                                <Label>{color_name}</Label><br />
                                <Label>{original_price}</Label><br />
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                <a href={product_url} target='_blank' rel="noreferrer"><Button color="primary" onClick={toggle}>Product Detail</Button></a>
                </ModalFooter>
            </Modal>
        </>
    )
}

export default FriendCard
