from os import sep, write
import requests
from bs4 import BeautifulSoup
import json
import re
from selenium import webdriver
import csv

data={}
data["cloth"] = []
data["cloth"].append({
    "name":"Scott",
    "price":"HKD200",
    "photo":"https://shoplineimg.com/56cb124503905552820000df/600bfec0176dad0032e905d0/1080x.jpg?",
    "color": "黑色 / 杏色",
    "size": "M / L",
    "material": "優質斜紋布", 
    "from": "www.fumble.com.hk",
    "brand":"fumble"
})

def process(url):
    #print("[info] processing url: [{}]".format(url))
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }

    driver = webdriver.Chrome("./chromedriver")
    #time.sleep(1)
    driver.implicitly_wait(5)
    driver.get(url)
    #print(driver.title)
    html=driver.page_source
    #print(html)
    driver.close()
    driver.quit()

    req = requests.get(url, headers)
    req.encoding= "utf8"
    soup = BeautifulSoup(req.content, 'html.parser')
    titleH1 = soup.find("h1", class_="Product-title")
    print(titleH1)
    priceDiv = soup.find("div", class_="global-primary")
    #print(priceDiv)
    prod_detail = soup.find()
    #print(prod_detail)
    fp = open("index.html", "w", encoding="utf8")
    fp.write(soup.prettify())
    titleH1 = soup.find("h1", class_="Product-title")
    priceDiv = soup.find("div", class_="global-primary")
    productDes = soup.find("div", class_="ProductDetail-description")
    product_images = soup.find_all("img", class_="no-js");
    linkStr = ""
    
    print(product_images)
    for product_image in product_images:
        # print(product_image)
        link = product_image.get("src")
        linkStr += link + ', '
        #print(link)
        price = re.sub(r"[\n\t\s]*", "",priceDiv.string)
        #print(price)

    t = productDes.text

    t1 = t.find("顔色：")
    t2 = t.find("特點：")
    t3 = t.find("尺寸：")
    t4 = t.find("產品：")
    t5 = t.find("材質：")

    color=(t[t1+3:t2].strip())
    size=(t[t3+3:t1].strip())
    mat=(t[t5+3:t3].strip())

    #print(color)
    #print(size)
    #print(mat)

    fp.write(productDes.prettify())

    data["cloth"].append({
        "name": titleH1.string,
        "price": price,
        "photo": linkStr,
        "color": color,
        "size": size,
        "material": mat, 
        "from" : "www.fumble.com.hk",
        "brand" :"fumble"
    })    

    fp.write(productDes.prettify())
        
    fp.write(titleH1.prettify())
    fp.write(priceDiv.prettify())
    fp.write(productDes.prettify())
    #print(productDes)
    #print("Writing index.html")

    csvfile = "TablaData.csv"
    list1=[titleH1.string,price,linkStr,color,size,mat,"www.fumble.com.hk","fumble",url]
    with open(csvfile, 'a', newline='')as fp:
        writer=csv.writer(fp)
        writer.writerow(list1)
        fp.close()

if __name__== '__main__': 
    csvfile = "TablaData.csv"
    with open(csvfile, 'a', newline='')as fp:
        writer=csv.writer(fp)
        writer.writerow(["name","price","photo","color","size","material","from","brand","url"])
        fp.close()
    print("[info] start processing...")
    with open("TablaLink.txt") as f:
        for url in f.readlines():
            url = url.strip()
            process(url)