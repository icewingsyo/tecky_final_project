from os import error, sep, write
import requests
from bs4 import BeautifulSoup
import json
import re
from selenium import webdriver
import csv

data={}
data["cloth"] = []

def process(url):
    #print("[info] processing url: [{}]".format(url))
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }

    driver = webdriver.Chrome("./chromedriver")
    #time.sleep(10)
    driver.implicitly_wait(100)
    if url is not None:
        driver.get(url)
        #print(driver.title)
        html=driver.page_source
        print(html)
        driver.close()
        driver.quit()

        req = requests.get(url, headers)
        req.encoding= "utf8"
        soup = BeautifulSoup(html, 'html.parser')
        priceDiv = soup.find("div", class_="global-primary dark-primary price-regular price js-price")
        if priceDiv is None:
            priceDiv = soup.find("div", class_="price-sale price js-price")
        

        print(priceDiv)
        #prod_detail = soup.find()
        #print(prod_detail)
        fp = open("index.html", "w", encoding="utf8")
        fp.write(soup.prettify())
        titleH1 = soup.find("h1", class_="Product-title")
        #print(titleH1)
        productDes = soup.find("div", class_="promotion-wrap")
        #print(productDes)
        product_images = soup.find_all("img", class_="img-responsive inline-block")
        #print(product_images)
        SizeSwatches= soup.find("select", class_="selectpicker js-selectpicker form-control form-control-sm form-control-inline ng-pristine ng-untouched ng-valid ng-scope ng-not-empty")
        #print(SizeSwatches)
        linkStr = ""
        #print(product_images)
        for product_image in product_images:
            #print(product_image)
            link = product_image.get("src")
            linkStr += link + ', '
            #print(link)
            price1 = re.sub(r"[\n\t\s\$]", "",priceDiv.string)
            #print(type(price1))
            #print(price1)
            price = price1.replace("HK","")
            #print(price)
        
        linkString = ""
        for SizeSwatche in SizeSwatches:
            linkage = SizeSwatche.get_text()
            #print(linkage)]
            if(linkage==''):
                linkage = 'F'
            linkString += linkage +','

        #SColorsses1=soup.find_all("select", class_="selectpicker js-selectpicker form-control form-control-sm form-control-inline ng-pristine ng-untouched ng-valid ng-scope ng-not-empty")
        SColorsses=soup.find_all("option")

        print(SColorsses)
        linkStringColor = ""
        for SColorss in SColorsses:
            linkageColor = SColorss.get_text()
            #print(linkage)]
            linkStringColor += linkageColor +','
        
        print(linkStringColor)

        SizeFinal=""
        y = linkStringColor
        print(y.split(",")[1:])
        for z in y.split(",")[1:]:
            if z != "":
            # print(z)
                SizeFinal += z + ', '
        print(SizeFinal)



        SColors="35,36,37,38,39,40,41"


        #print(Colors)
        #print(html)

        ScrapMaterial="Unknown"

        #print(Material)

        t = productDes.text

        t1 = t.find("顔色：")
        t2 = t.find("特點：")
        t3 = t.find("尺寸：")
        t4 = t.find("產品：")
        t5 = t.find("材質：")

        color=(t[t1+3:t2].strip())
        size=(t[t3+3:t1].strip())
        mat=(t[t5+3:t3].strip())

        #print(color)
        #print(size)
        #print(mat)

        fp.write(productDes.prettify())
        
        data["cloth"].append({
            "name": titleH1.string,
            "price": price,
            "photo": linkStr,
            "color": SColors,
            "size": linkStr,
            "material": ScrapMaterial, 
            "from" : "https://www.supergahk.com/",
            "brand" :"Supergahk",
            "url":"url"
        })

        fp.write(productDes.prettify())
        fp.write(titleH1.prettify())
        fp.write(priceDiv.prettify())
        fp.write(productDes.prettify())
        linkStringColorss=fp.write(linkStringColor.replace(",,",''))
        fp.write(linkString)
        fp.write(ScrapMaterial)
        #fp.write(SizeSwatche.prettify())
        #print(productDes)
        #print("Writing index.html")

    csvfile = "SpSupergahkMale.csv"
    list1=[titleH1.string,price,linkStr,linkString,SizeFinal,ScrapMaterial,"https://www.supergahk.com/","Supergahk",url,"Male","Shoes","Adult"]
    with open(csvfile, 'a', newline='')as fp:
        writer=csv.writer(fp)
        writer.writerow(list1)
        fp.close()

if __name__== '__main__': 
    csvfile = "SpSupergahkMale.csv"
    with open(csvfile, 'a', newline='')as fp:
        writer=csv.writer(fp)
        writer.writerow(["name","price","photo","color","size","material","from","brand","url","Gender","Type","Age"])
        fp.close()
    print("[info] start processing...")
    with open("SpSupergahkMaleLink.txt") as f:
        try:
            for url in f.readlines():
                url = url.strip()
                process(url)
        except (RuntimeError,TimeoutError,TypeError):
            pass
            print("Oops! there is RuntimeError, TimeoutError & TypeError. Try again")
        except(OSError):
            pass 
            print("Oops!It have OSError")